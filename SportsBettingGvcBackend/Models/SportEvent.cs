﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportsBettingGvcBackend.Models
{
    public class SportEvent
    {
        private Single _OddsForFirstTeam;
        private Single _OddsForDraw;
        private Single _OddsForSecondTeam;

        [Key]
        public int EventID { get; set; }

        [Column(TypeName = "nvarchar(200)")]
        public string EventName { get; set; }

        [Column(TypeName = "real")]
        [DefaultValue(1)]
        public Single OddsForFirstTeam {
            get
            {
                return (Single)Math.Round(this._OddsForFirstTeam, 2);
            }

            set
            {
                if (value >= 1)
                {
                    this._OddsForFirstTeam = value;
                }
            }
        }

        [Column(TypeName = "real")]
        [DefaultValue(1)]
        public Single OddsForDraw {
            get
            {
                return (Single)Math.Round(this._OddsForDraw, 2);
            }
            set
            {
                if(value >= 1)
                {
                    this._OddsForDraw = value;
                }

            }
        }

        [Column(TypeName = "real")]
        [DefaultValue(1)]
        public Single OddsForSecondTeam {
            get
            {
                return (Single)Math.Round(this._OddsForSecondTeam, 2);
            }

            set
            {
                if (value >= 1)
                {
                    this._OddsForSecondTeam = value;

                }
            }
        }

        [Required]
        [Column(TypeName = "datetime")]
        public DateTime EventStartDate { get; set; }
    }
}
