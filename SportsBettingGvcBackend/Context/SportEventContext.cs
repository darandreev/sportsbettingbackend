﻿using Microsoft.EntityFrameworkCore;
using SportsBettingGvcBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsBettingGvcBackend.Context
{
    public class SportEventContext : DbContext 
    {


            public SportEventContext(DbContextOptions<SportEventContext> options) : base(options)
        {

        }

        public DbSet<SportEvent> SportEvent { get; set; }
    }
}
