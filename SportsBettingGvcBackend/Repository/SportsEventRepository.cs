﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsBettingGvcBackend.Context;
using SportsBettingGvcBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace SportsBettingGvcBackend.Repository
{
    public class SportsEventRepository : ISportEventsRepository
    {
        private readonly SportEventContext _sportEventContext;

        public SportsEventRepository(SportEventContext sportEventContext)
        {
            _sportEventContext = sportEventContext;
        }


        public void DeleteSportEvent(SportEvent sportEvent) 
                    => _sportEventContext.SportEvent.Remove(sportEvent);

        public Task<SportEvent> GetOneSportEvent(int id) 
                    => _sportEventContext.SportEvent.SingleOrDefaultAsync(s => s.EventID == id);

        public Task<List<SportEvent>> GetSportEvent() 
                    => _sportEventContext.SportEvent.ToListAsync();

        public void AddSportEvent(SportEvent sportEvent) 
                    => _sportEventContext.SportEvent.Add(sportEvent);

        public void UpdateSportEvent(SportEvent sportEvent)
                    => _sportEventContext.Entry(sportEvent).State = EntityState.Modified;

        public Task SaveChanges() 
                    => _sportEventContext.SaveChangesAsync();

        public bool SportEventExists(int id) 
                    => _sportEventContext.SportEvent.Any(e => e.EventID == id);
    }
}
