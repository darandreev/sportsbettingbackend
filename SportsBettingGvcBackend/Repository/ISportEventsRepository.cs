﻿using Microsoft.AspNetCore.Mvc;
using SportsBettingGvcBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsBettingGvcBackend.Repository
{
   public interface ISportEventsRepository
    {
        Task<List<SportEvent>> GetSportEvent();
        Task<SportEvent> GetOneSportEvent(int id);
        void UpdateSportEvent(SportEvent sportEvent);
        void AddSportEvent(SportEvent sportEvent);
        Task SaveChanges();
        void DeleteSportEvent(SportEvent sportEvent);
        bool SportEventExists(int id);

    }
}
