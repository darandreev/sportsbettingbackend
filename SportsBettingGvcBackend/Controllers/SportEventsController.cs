﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsBettingGvcBackend.Context;
using SportsBettingGvcBackend.Models;
using SportsBettingGvcBackend.Repository;

namespace SportsBettingGvcBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/SportEvents")]
    public class SportEventsController : Controller
    {
      
        private readonly ISportEventsRepository _sportEventsRepository;

        public SportEventsController(ISportEventsRepository sportEventsRepository)
        {
            _sportEventsRepository = sportEventsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<SportEvent>> GetSportEvent()
        {
            return await _sportEventsRepository.GetSportEvent();
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> PutSportEvent([FromRoute] int id, [FromBody] SportEvent sportEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sportEvent.EventID)
            {
                return BadRequest();
            }

            _sportEventsRepository.UpdateSportEvent(sportEvent);

            try
            {
                await _sportEventsRepository.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SportEventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> PostSportEvent([FromBody] SportEvent sportEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (sportEvent.OddsForDraw < 1 || sportEvent.OddsForFirstTeam < 1 || sportEvent.OddsForSecondTeam < 1)
            {
                return BadRequest();
            }

            _sportEventsRepository.AddSportEvent(sportEvent);
            await _sportEventsRepository.SaveChanges();

            return CreatedAtAction("GetSportEvent", new { id = sportEvent.EventID }, sportEvent);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSportEvent([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var sportEvent = await _sportEventsRepository.GetOneSportEvent(id);

            if (sportEvent == null)
            {
                return NotFound();
            }

            _sportEventsRepository.DeleteSportEvent(sportEvent);
            await _sportEventsRepository.SaveChanges();

            return Ok(sportEvent);
        }

        private bool SportEventExists(int id)
        {
            return _sportEventsRepository.SportEventExists(id);
        }
    }
}