using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using SportsBettingGvcBackend.Context;
using SportsBettingGvcBackend.Controllers;
using SportsBettingGvcBackend.Models;
using SportsBettingGvcBackend.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;



namespace SportsBettingUnitTest
{

    public class SportEventControllerTest
    {

        private Mock<ISportEventsRepository> sportEventRepository;
        private SportEventsController sportEventsController;

        public SportEventControllerTest()
        {
            sportEventRepository = new Mock<ISportEventsRepository>();
            sportEventsController = new SportEventsController(sportEventRepository.Object);
        }

        [Fact]
        public async void Return_Sport_Events_Successfully()
        {
            var mockSportEventList = new List<SportEvent>
            {
                new SportEvent { EventID = 1, EventStartDate = new DateTime() },
                new SportEvent { EventID = 2, EventStartDate = new DateTime() }
            };

            sportEventRepository.Setup(repo => repo.GetSportEvent())
                                .Returns(Task.FromResult(mockSportEventList));

            var result = await sportEventsController.GetSportEvent();
            var viewResult = Assert.IsType<List<SportEvent>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<SportEvent>>(viewResult);
            Assert.Equal(2, model.Count());
        }

        [Fact]
        public async Task SportEvent_Delete_Returns_NotFound_WhenNoIdProvided()
        {
            var result = await sportEventsController.DeleteSportEvent(1000);

            var viewResult = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task SportEvent_Update_Return_BadRequest_WhenIdIsDifferent()
        {
            var sportEventMock = new SportEvent()
            {
                EventID = 5,
                EventStartDate = new DateTime()
            };

            int id = 7;

            var result = await sportEventsController.PutSportEvent(id, sportEventMock);

            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task SportEvent_Add_WhenOddsAreLessThanOne()
        {
            var sportEventMock = new SportEvent()
            {
                EventID = 5,
                EventStartDate = new DateTime(),
                OddsForDraw = 0,
                OddsForFirstTeam = 0,
                OddsForSecondTeam = 0
            };

            var result = await sportEventsController.PostSportEvent(sportEventMock);

            Assert.IsType<BadRequestResult>(result);
        }
    }
}

